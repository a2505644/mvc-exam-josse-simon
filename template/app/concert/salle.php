<h2>Ajouter une nouvelle salle</h2>
<?php
$textButton = 'Ajouter'; ?>
<form action="" method="post" novalidate class="wrapform">
    <?php echo $form->label('title'); ?>
    <?php echo $form->input('title','text', $user->title ?? '') ?>
    <?php echo $form->error('title'); ?>

    <?php echo $form->label('maxuser'); ?>
    <?php echo $form->input('maxuser','number', $user->maxuser ?? '') ?>
    <?php echo $form->error('maxuser'); ?>

    <?php echo $form->submit('submitted', $textButton); ?>
</form>