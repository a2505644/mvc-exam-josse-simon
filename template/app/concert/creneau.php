<h2>Ajouter un nouveau créneau</h2>
<?php
$textButton = 'Ajouter'; ?>
<form action="" method="post" novalidate class="wrapform">
    <?php echo $form->label('salle'); ?>
    <?php echo $form->input('id_salle','number', $creneau->id_salle ?? '') ?>
    <?php echo $form->error('id_salle'); ?>

    <?php echo $form->label('start_at'); ?>
    <?php echo $form->input('start_at','time', $creneau->start_at ?? '') ?>
    <?php echo $form->error('start_at'); ?>

    <?php echo $form->label('nbrehours'); ?>
    <?php echo $form->input('nbrehours','number', $creneau->nbrehours ?? '') ?>
    <?php echo $form->error('nbrehours'); ?>

    <?php echo $form->submit('submitted', $textButton); ?>
</form>