<h2>Ajouter un nouvel user</h2>
<?php
$textButton = 'Ajouter'; ?>
<form action="" method="post" novalidate class="wrapform">
    <?php echo $form->label('nom'); ?>
    <?php echo $form->input('nom','text', $abonne->nom ?? '') ?>
    <?php echo $form->error('nom'); ?>

    <?php echo $form->label('email'); ?>
    <?php echo $form->input('email','email', $abonne->email ?? '') ?>
    <?php echo $form->error('email'); ?>

    <?php echo $form->submit('submitted', $textButton); ?>
</form>