<h2><?= $salle->getTitle(); ?></h2>

<h2>Ajouter un nouvel user à une salle</h2>
<?php
$textButton = 'Ajouter'; ?>
<form action="" method="post" novalidate class="wrapform">
    <?php echo $form->label('id_creneau'); ?>
    <?php echo $form->input('id_creneau','number', $creneau->id_creneau ?? '') ?>
    <?php echo $form->error('id_creneau'); ?>

    <?php echo $form->label('id_user'); ?>
    <?php echo $form->input('id_user','number', $creneau->id_user ?? '') ?>
    <?php echo $form->error('id_user'); ?>

    <?php echo $form->submit('submitted', $textButton); ?>
</form>