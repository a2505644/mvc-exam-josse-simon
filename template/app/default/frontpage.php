<h1 style="text-align: center;font-size:33px;margin: 100px 0;color:#A67153;">
    <?= $message; ?>
</h1>

<section id="liste1">
    <div class="wrap">
        <h1>Liste des users :</h1>

        <table class="user">
            <thead>
            <tr>
                <th>Id</th>
                <th>Nom</th>
                <th>Email</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($users as $user) { ?>
                <tr>
                    <td><?= $user->getId(); ?></td>
                    <td><?= $user->getNom(); ?></td>
                    <td><?= $user->getEmail(); ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</section>

<section id="liste2">
    <div class="wrap">
        <h1>Liste des salles :</h1>

        <table class="salle">
            <thead>
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Max user</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($salles as $salle) { ?>
                <tr>
                    <td><?= $salle->getId(); ?></td>
                    <td><?= $salle->getTitle(); ?></td>
                    <td><?= $salle->getMaxuser(); ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</section>

<section id="liste3">
    <div class="wrap">
        <h1>Liste des créneaux :</h1>

        <table class="creneau">
            <thead>
            <tr>
                <th>Id</th>
                <th>Id-salle</th>
                <th>Max user</th>
                <th>Nbre heures</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($creneaux as $creneau) { ?>
                <tr>
                    <td><?= $creneau->getId(); ?></td>
                    <td><?= $creneau->getSalle(); ?></td>
                    <td><?= $creneau->getStart_at(); ?></td>
                    <td><?= $creneau->getNbrehours(); ?></td>
                    <td><a href="<?php echo $view->path('single', array('id'=> $creneau->getId())) ?>">Voir détail</a></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</section>