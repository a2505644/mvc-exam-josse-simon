<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Framework Pédagogique MVC6</title>
    <?php echo $view->add_webpack_style('app'); ?>
  </head>
  <body>
    <header id="masthead">
      <nav>
          <ul>
              <li><a href="<?= $view->path(''); ?>">Home</a></li>
              <li><a href="<?= $view->path('user'); ?>">User</a></li>
              <li><a href="<?= $view->path('salle'); ?>">Salle</a></li>
              <li><a href="<?= $view->path('creneau'); ?>">Créneau</a></li>
          </ul>
      </nav>
    </header>
<div class="wrap" style="color: green">
    <?php foreach ($view->getFlash() as $flash) {
        echo '<p class="'.$flash['type'].'">'.$flash['message'].'</p>';
    }
    ?>
</div>

    <div class="container">
        <?= $content; ?>
    </div>

    <footer id="colophon">
        <div class="wrap">
            <p>MVC 6 - Framework Pédagogique.</p>
        </div>
    </footer>
  <?php echo $view->add_webpack_script('app'); ?>
  </body>
</html>
