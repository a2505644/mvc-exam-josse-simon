<?php

namespace App\Controller;

use Core\Kernel\AbstractController;
use App\Model\UserModel;
use App\Model\SalleModel;
use App\Model\CreneauModel;
use App\Model\UserCreneauModel;
use App\Service\Form;
use App\Service\Validation;

/**
 *
 */
class DefaultController extends AbstractController
{
    public function index()
    {
        $message = 'Bienvenue sur le framework MVC';
        $users = UserModel::all();
        $salles = SalleModel::all();
        $creneaux = CreneauModel::all();
        $this->render('app.default.frontpage',array(
            'message' => $message,
            'users' => $users,
            'salles' => $salles,
            'creneaux' => $creneaux,
        ));
    }

    public function single($id) {
        $user = $this->getUserByIdOr404($id);
        $salle = $this->getSalleByIdOr404($id);
        $creneau = $this->getCreneauByIdOr404($id);
        $user_creneau = $this->getUserCreneauByIdOr404($id);

        $errors = [];
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors = $this->validate($v,$post);
            if($v->isValid($errors)) {
                UserCreneauModel::insert($post);
                $this->addFlash('success', 'Un nouvel user a bien été ajouté à la salle');
                $this->redirect('');
            }
        }
        $form = new Form($errors);

        $this->render('app.concert.single',array(
            'user' => $user,
            'salle' => $salle,
            'creneau' => $creneau,
            'user_creneau' => $user_creneau,
            'form' => $form,
        ));
    }

    private function getUserByIdOr404($id)
    {
        $user = UserModel::findById($id);
        if(empty($user)) {
            $this->Abort404();
        }
        return $user;
    }

    private function getSalleByIdOr404($id)
    {
        $salle = SalleModel::findById($id);
        if(empty($salle)) {
            $this->Abort404();
        }
        return $salle;
    }

    private function getCreneauByIdOr404($id)
    {
        $creneau = CreneauModel::findById($id);
        if(empty($creneau)) {
            $this->Abort404();
        }
        return $creneau;
    }

    private function getUserCreneauByIdOr404($id)
    {
        $user_creneau = UserCreneauModel::findById($id);
        if(empty($user_creneau)) {
            $this->Abort404();
        }
        return $user_creneau;
    }

    private function validate($v,$post)
    {
        $errors = [];
        $errors['id_creneau'] = $v->textValid($post['id_creneau'], 'id_creneau',1, 11);
        $errors['id_user'] = $v->textValid($post['id_user'], 'id_user',1, 11);
        return $errors;
    }

    /**
     * Ne pas enlever
     */
    public function Page404()
    {
        $this->render('app.default.404');
    }
}
