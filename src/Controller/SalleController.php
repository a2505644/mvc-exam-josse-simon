<?php

namespace App\Controller;

use App\Model\SalleModel;
use App\Service\Form;
use App\Service\Validation;

class SalleController extends BaseController
{

    public function add() {
        $errors = [];
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors = $this->validate($v,$post);
            if($v->isValid($errors)) {
                SalleModel::insert($post);
                $this->addFlash('success', 'Une nouvelle salle a bien été ajouté');
                $this->redirect('');
            }
        }
        $form = new Form($errors);
        $this->render('app.concert.salle', array(
            'form' => $form,
        ));
    }

    private function validate($v,$post)
    {
        $errors = [];
        $errors['title'] = $v->textValid($post['title'], 'title',2, 255);
        $errors['maxuser'] = $v->textValid($post['maxuser'], 'maxuser',1, 6);
        return $errors;
    }
}