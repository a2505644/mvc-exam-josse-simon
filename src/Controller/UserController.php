<?php

namespace App\Controller;

use App\Model\UserModel;
use App\Service\Form;
use App\Service\Validation;

class UserController extends BaseController
{

    public function add() {
        $errors = [];
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors = $this->validate($v,$post);
            if($v->isValid($errors)) {
                UserModel::insert($post);
                $this->addFlash('success', 'Un nouvel user a bien été ajouté');
                $this->redirect('');
            }
        }
        $form = new Form($errors);
        $this->render('app.concert.user', array(
            'form' => $form,
        ));
    }

    private function validate($v,$post)
    {
        $errors = [];
        $errors['nom'] = $v->textValid($post['nom'], 'nom',2, 255);
        $errors['email'] = $v->emailValid($post['email']);
        return $errors;
    }
}