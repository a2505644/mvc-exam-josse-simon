<?php

namespace App\Controller;

use App\Model\CreneauModel;
use App\Service\Form;
use App\Service\Validation;

class CreneauController extends BaseController
{

    public function add() {
        $errors = [];
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors = $this->validate($v,$post);
            if($v->isValid($errors)) {
                CreneauModel::insert($post);
                $this->addFlash('success', 'Un nouveau créneau a bien été ajouté');
                $this->redirect('');
            }
        }
        $form = new Form($errors);
        $this->render('app.concert.creneau', array(
            'form' => $form,
        ));
    }


    private function validate($v,$post)
    {
        $errors = [];
        $errors['id_salle'] = $v->textValid($post['id_salle'], 'id_salle',1, 5);
        $errors['nbrehours'] = $v->textValid($post['nbrehours'], 'nbrehours',1, 2);
        return $errors;
    }
}