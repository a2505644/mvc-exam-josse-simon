<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class UserCreneauModel extends AbstractModel
{
    protected static $table = 'creneau_user';

    protected $id;
    protected $id_creneau;
    protected $id_user;
    protected $created_at;

    // insert
    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (id_creneau, id_user, created_at) VALUES (?,?,NOW())",
            [$post['id_creneau'], $post['id_user']]
        );
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getId_creneau()
    {
        return $this->id_creneau;
    }

    /**
     * @return mixed
     */
    public function getId_user()
    {
        return $this->id_user;
    }

    /**
     * @return mixed
     */
    public function getCreated_at()
    {
        return $this->created_at;
    }


}