<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class CreneauModel extends AbstractModel
{
    protected static $table = 'creneau';

    protected $id;
    protected $id_salle;
    protected $start_at;
    protected $nbrehours;

    // insert
    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (id_salle, start_at, nbrehours) VALUES (?,?,?)",
            [$post['id_salle'], $post['start_at'], $post['nbrehours']]
        );
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getSalle()
    {
        return $this->id_salle;
    }

    /**
     * @return mixed
     */
    public function getStart_at()
    {
        return $this->start_at;
    }

    /**
     * @return mixed
     */
    public function getNbrehours()
    {
        return $this->nbrehours;
    }


}